<?php
if ($_SERVER['HTTP_HOST'] == 'localhost') {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

$s = isset($_GET['s']) ? $_GET['s'] : 0;
$t = isset($_GET['t']) ? $_GET['t'] : 'hello';
$headers = getallheaders();

usleep($s);

header('Content-Type: application/json');
echo json_encode(array_merge($headers, array(
        's' => $s,
        't' => $t
)));
