<?php
// if ($_SERVER['HTTP_HOST'] == 'localhost') {
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// }
require_once __DIR__ . '/vendor/autoload.php';

use Jaeger\Config;
use OpenTracing\Formats;

$headers = getallheaders();

$config = Config::getInstance();
$config::$propagator = \Jaeger\Constants\PROPAGATOR_ZIPKIN;
// init server span start
$tracer = $config->initTrace('Istio', 'jaeger-agent.istio-system:6831');
$spanContext = $tracer->extract(Formats\TEXT_MAP, $headers);

$serverSpan = $tracer->startSpan('Istio1', [
        'child_of' => $spanContext
]);
$tracer->inject($serverSpan->getContext(), Formats\TEXT_MAP, $_SERVER);
// print_r($_SERVER);

// client span1 start
$clientTrace = $config->initTrace('Istio1 HTTP');
$injectTarget = [];
$spanContext = $clientTrace->extract(Formats\TEXT_MAP, $_SERVER);
$clientSapn = $clientTrace->startSpan('Istio1', [
        'child_of' => $spanContext
]);
$clientTrace->inject($clientSapn->spanContext, Formats\TEXT_MAP, $injectTarget);

$req_headers = array();
foreach ($injectTarget as $header => $value) {
    $req_headers[] = "$header: $value";
}

$opts = array(
        'http' => array(
                'protocol_version' => '1.1',
                'method' => 'GET',
                'header' => array_merge($req_headers,
                        array(
                                'Connection: close'
                        ))
        )
);
$clientSapn->setTags([
        "http.url" => "sleeping-echoserver"
]);

$json = file_get_contents('http://sleeping-echoserver/?t=ciao', FALSE,
        stream_context_create($opts));

$clientSapn->setTags([
        "http.status_code" => 666
]);
// client span1 end
// server span end
$serverSpan->finish();
// trace flush
$config->flush();

// # END PASTED STUFF

echo "<pre>### RECEIVED HEADERS\n";
print_r($headers);
echo "### END HEADERS</pre>\n";

echo "<pre>### RECEIVED RESPONSE\n";
print_r(json_decode($json));
echo "### END RECEIVED RESPONSE</pre>\n";
